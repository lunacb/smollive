package main

import (
	"bytes"
	"context"
	"crypto/sha256"
	"errors"
	"flag"
	"fmt"
	"github.com/fsnotify/fsnotify"
	"golang.org/x/net/html"
	"io"
	"io/fs"
	"log"
	"net/http"
	"nhooyr.io/websocket"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
)

var injected_html string =
`

<!-- Injected by smollive (https://codeberg.org/lunacb/smollive) -->
<script src="%s/script.js"></script>


`
var update_script string =
`
(function(){
	// the test argument is a  function that takes a resource URL as argument,
	// and returns true or false depending on whether the associated element
	// should be reloaded.
	// Returns true if any elements were updated, false otherwise.
	function reloadElements(test) {
		tags = [
			[ "link",   "href" ],
			[ "img",    "src"  ],
			[ "audio",  "src"  ],
			[ "video",  "src"  ],
			[ "source", "src"  ],
		];
		var found = false;
		var placeholder = document.createElement("div");
		for(var i = 0; i < tags.length; i++) {
			elems = Array.from(document.getElementsByTagName(tags[i][0]));
			attrib = tags[i][1]
			for(var j = 0; j < elems.length; j++) {
				var elem = elems[j];
				var url = new URL(elem[attrib], document.location)
				if(test(url))  {
					found = true;

					// Remove the element.
					var parent = elem.parentElement;
					parent.replaceChild(placeholder, elem);

					var params = new URLSearchParams(url.search);
					params.set("%s", Date.now().valueOf())
					url.search = params.toString();
					elem[attrib] = url;

					// Put the element back.
					parent.appendChild(elem);
					parent.replaceChild(elem, placeholder);
				}
			}
		}

		return found;
	}

	// Add our anti-caching artifact at page-load for good measure.
	reloadElements((_) => true);

	(new WebSocket("ws://" + window.location.host + "%s/socket")).onmessage = function(msg) {
		var paths = msg.data.split("\n").filter(a => a.length);

		// First, try to hot-reload whatever html elements rely on the
		var found = reloadElements((url) => {
			var abs_path = "/" + url.pathname.split("/").filter(a => a.length).join("/");
			return(url.host == window.location.host && paths.includes(abs_path))
		});

		// Just reload the entire page if we can't figure out what was changed
		// or if the server wants us to. This might be unnecessary, because the
		// modified resource might not be relevant to us, but it's easier than
		// trying to figure out if it is.
		if(!found) {
			window.location.reload();
		}
	};
})();
`

type RecursiveWatcher struct {
	Watcher *fsnotify.Watcher

	EmptyEvents chan fsnotify.Event
	EmptyErrors chan error
}

type SumMap = map[string][]byte

func recursiveWatcher(root string, sums *SumMap) (RecursiveWatcher, error) {
	walker, err := fsnotify.NewWatcher()
	if err != nil { return RecursiveWatcher{}, err }
	res := RecursiveWatcher{ Watcher: walker }
	res.Add(root, sums)
	return res, nil
}

func emptyRecursiveWatcher() RecursiveWatcher {
	return RecursiveWatcher{
		Watcher: nil,
		EmptyEvents: make(chan fsnotify.Event),
		EmptyErrors: make(chan error),
	}
}

// Recursively watch all directories for changes.
func (r *RecursiveWatcher) Add(root string, sums *SumMap) {
	err := filepath.WalkDir(root, func(path string, d fs.DirEntry, err error) error {
		if(err != nil) { return nil }
		if(!d.IsDir()) {
			if sums != nil { cksum(path, sums) }
			return nil
		}

		if err := r.Watcher.Add(path); err != nil {
			log.Printf("Failed to watch the directory \"%s\": %s", path, err)
		}

		return nil
	})

	if err != nil {
		log.Printf("Failed to watch the directory \"%s\": %s", root, err)
	}
}

func (r *RecursiveWatcher) Close() {
	r.Watcher.Close()
}

func (r *RecursiveWatcher) Events() chan fsnotify.Event {
	if r.Watcher != nil {
		return r.Watcher.Events
	} else {
		return r.EmptyEvents
	}
}

func (r *RecursiveWatcher) Errors() chan error {
	if r.Watcher != nil {
		return r.Watcher.Errors
	} else {
		return r.EmptyErrors
	}
}

func (r *RecursiveWatcher) HandleEvent(event fsnotify.Event) {
	if event.Has(fsnotify.Create) {
		// If a new directory was created, begin watching it.
		if stat, err := os.Stat(event.Name); err == nil && stat.IsDir() {
			r.Add(event.Name, nil)
		}
	}
}

func isHtml(basename string, isdir bool, file io.ReadSeeker) bool {
	if(isdir) { return false }
	if strings.HasSuffix(basename, ".html") { return true }

	// Read the first 512 bytes of the file then seek back to the beginning.
	bytes := []byte{}
	if _, err := io.ReadAtLeast(file, bytes, 512); err != nil &&
			err != io.EOF &&
			err != io.ErrUnexpectedEOF &&
			err != io.ErrShortBuffer {
		log.Printf("Failed to read: %s", err)
		return false
	}
	if _, err := file.Seek(0, io.SeekStart); err != nil {
		log.Printf("Failed to seek: %s", err)
	}

	return (http.DetectContentType(bytes[:]) == "text/html")
}

func rmConn(conns []*websocket.Conn, i int) {
	conns[i] = conns[len(conns) - 1]
	conns = conns[:len(conns) - 1]
}

func cksum(path string, sums *SumMap) bool {
	f, err := os.Open(path)
	if err != nil {
		log.Printf("Failed to open file: %s", err)
		return true
	}
	defer f.Close()

	h := sha256.New()
	if _, err = io.Copy(h, f); err != nil {
		log.Printf("Failed to read from file: %s", err)
		return  true
	}
	sum := h.Sum(nil)

	old, ok := (*sums)[path]
	(*sums)[path] = sum
	if ok && bytes.Compare(old[:], sum[:]) == 0 {
		return false
	}

	return true
}

func main() {
	var root, src_path, src_cmd, src_cwd string
	var port int
	var force_reload bool
	var sleep_time time.Duration

	var api_path string
	var cache_param_name string

	var no_checksums bool

	{
		const (
			Root = "The directory to serve"
			SrcPath = "A source directory that builds to the root directory"
			SrcCmd = "An update command run when the source directory changes"
			SrcCwd = "The working directory of the update command. Defaults to the source directory"
			Port = "The port used by the HTTP server"
			ForceReload = "Forces the page to be fully reloaded for every file update"
			SleepTime = "How long to wait before propagating file updates after detecting changes"
			ApiPath = "Base path used to provide resources needed for client-side updating"
			CacheParamName = "Name of a URL query paramater used to prevent caching"
			NoChecksums = "If set, the server will not use file checksums to decide what's been changed"
		)
		flag.StringVar(&root, "root", ".", Root)
		flag.StringVar(&src_path, "src-path", "", SrcPath)
		flag.StringVar(&src_path, "P", "", SrcPath)
		flag.StringVar(&src_cmd, "src-cmd", "", SrcCmd)
		flag.StringVar(&src_cmd, "C", "", SrcCmd)
		flag.StringVar(&src_cwd, "src-cwd", "", SrcCwd)
		flag.StringVar(&src_cwd, "D", "", SrcCwd)
		flag.StringVar(&root, "r", ".", Root)
		flag.IntVar(&port, "port", 8888, Port)
		flag.IntVar(&port, "p", 8888, Port)
		flag.BoolVar(&force_reload, "force-reload", false, ForceReload)
		flag.BoolVar(&force_reload, "f", false, ForceReload)
		flag.BoolVar(&no_checksums, "c", false, NoChecksums)
		flag.BoolVar(&no_checksums, "no-checksums", false, NoChecksums)
		flag.DurationVar(&sleep_time, "sleep-time", 100 * time.Millisecond, SleepTime)
		flag.DurationVar(&sleep_time, "s", 100 * time.Millisecond, SleepTime)
		flag.StringVar(&api_path, "api-path", "/_smollive", ApiPath)
		flag.StringVar(&cache_param_name, "cache-param-name", "_smollive_anticache", CacheParamName)
	}

	flag.Parse()

	if src_path != "" && src_cwd == "" { src_cwd = src_path }

	hostname, err := os.Hostname()
	if err != nil {
		hostname = "localhost"
	}

	server_start_time := time.Now()

	log.Println("Server started!")
	log.Printf("  Location:        http://%s:%d/", hostname, port)
	log.Printf("  Root path:       %s", root)
	log.Printf("  Source path:     %s", src_path)
	log.Printf("  Source command:  %s", src_cmd)
	log.Printf("  Source cwd:      %s", src_cwd)
	log.Printf("  Forced reload:   %t", force_reload)
	log.Printf("  Sleep time:      %s", sleep_time)
	log.Printf("  API path:        %s", api_path)

	injected_html = fmt.Sprintf(injected_html, api_path)
	update_script = fmt.Sprintf(update_script, cache_param_name, api_path)

	connCh := make(chan *websocket.Conn)

	go func() {
		conns := []*websocket.Conn{}
		defer func() {
			for _, conn := range(conns) {
				conn.Close(websocket.StatusNormalClosure, "bye!")
			}
		}()

		var err error
		var root_sums SumMap
		var root_watcher RecursiveWatcher

		if !no_checksums {
			root_sums = make(SumMap) 
			root_watcher, err = recursiveWatcher(root, &root_sums)
		} else {
			root_watcher, err = recursiveWatcher(root, nil)
		}

		if err != nil {
			log.Printf("Failed to create a watcher for \"%s\": %s", root, err)
			os.Exit(1)
		}
		defer root_watcher.Close()

		var src_watcher RecursiveWatcher
		if src_path != "" {
			var err error
			src_watcher, err = recursiveWatcher(src_path, nil)
			if err != nil {
				log.Printf("Failed to create a watcher for \"%s\": %s", root, err)
				os.Exit(1)
			}
			defer src_watcher.Close()
		} else {
			src_watcher = emptyRecursiveWatcher()
		}

		timestartCh := make(chan byte)
		timeoutCh := make(chan byte)
		go func() {
			for range(timestartCh) {
				time.Sleep(sleep_time)
				timeoutCh <- 1
			}
		}()

		waiting := false
		pending_updates := []string{}

		cmd_running := false
		cmd_pending := false
		cmdstartCh := make(chan byte)
		cmdendCh := make(chan byte)

		go func() {
			for range(cmdstartCh) {
				cmd := exec.Command("/bin/sh", "-c", src_cmd)
				cmd.Dir = src_cwd
				cmd.Stdout = os.Stdout
				cmd.Stderr = os.Stderr
				if err := cmd.Run(); err != nil {
					log.Printf("Failed to execute \"%s\": %s", src_cmd, err)
				}
				cmdendCh <- 1
			}
		}()

		for {
			select {
			case <-timeoutCh:
				waiting = false
				//  Transform the pending updates list by removing elements we
				//  decide don't need to be included because of an identical
				//  checksum to one stored and transform paths into something
				//  more presentable for web browsers.
				for i := len(pending_updates) - 1; i >= 0; i -= 1 {
					pending := pending_updates[i]
					if pending != "" {
						if !no_checksums && !cksum(pending, &root_sums) {
							pending_updates[i] = pending_updates[len(pending_updates)-1]
							pending_updates = pending_updates[:len(pending_updates)]
							continue
						}

						// Create a cleaned-up absolute pathname relative to
						// the http server root.
						rel, err := filepath.Rel(root, pending)
						if err != nil {
							log.Println("There is probably a bug")
							rel = ""
						} else {
							rel = filepath.ToSlash(filepath.Clean(rel))
							if rel[0] != '/' { rel = "/" + rel }
						}

						pending_updates[i] = rel

						log.Printf("File update detected: %s", rel)
					}
				}

				//  Iterate over each connection and write all the paths to them.
				for i := len(conns) - 1; i >= 0; i -= 1 {
					conn := conns[i]

					ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
					defer cancel()

					w, err := conn.Writer(ctx, websocket.MessageText)
					if err != nil {
						rmConn(conns, i)
						continue
					}

					err = nil
					for _, pending := range(pending_updates) {
						if _, err = io.WriteString(w, pending); err != nil {
							break
						}
						if _, err = io.WriteString(w, "\n"); err != nil {
							break
						}
					}

					if err != nil  {
						rmConn(conns, i)
						continue
					}

					if err = w.Close(); err != nil {
						rmConn(conns, i)
						continue
					}
				}

				// Start the command if necessary.
				if cmd_pending {
					cmd_pending = false
					if !cmd_running {
						cmd_running = true
						cmdstartCh <- 1
					}
				}

				// Reset the pending list.
				pending_updates = pending_updates[:0]

			case <-cmdendCh:
				cmd_running = false

			case e, ok := <-root_watcher.Events():
				if !ok {
					log.Printf("Watcher for \"%s\" died", root)
					return
				}
				root_watcher.HandleEvent(e)

				var path string

				if force_reload {
					path = ""
				} else {
					path = e.Name
				}

				// Check if the file is already pending and add it to the
				// list if it isn't.
				found := false
				for _, pending := range(pending_updates) {
					if(pending == path) {
						found = true
						break
					}
				}
				if !found {
					pending_updates = append(pending_updates, path)
				}

				// Start a timer to tell us when to apply the changes if
				// one isn't started already.
				if !waiting {
					waiting = true
					timestartCh <- 1
				}

			case err, ok := <-root_watcher.Errors():
				if !ok {
					log.Printf("Watcher for \"%s\" died", root)
					return
				}

				log.Printf("Watcher error for \"%s\": %s", root, err)

			case e, ok := <-src_watcher.Events():
				if !ok {
					log.Printf("Watcher for \"%s\" died", src_path)
					return
				}
				src_watcher.HandleEvent(e)

				if src_cmd != "" {
					// Remember that we're waiting to run the command.
					cmd_pending = true

					// Start a timer to tell us when to apply the changes if
					// one isn't started already.
					if !waiting {
						waiting = true
						timestartCh <- 1
					}
				}

			case err, ok := <-src_watcher.Errors():
				if !ok {
					log.Printf("Watcher for \"%s\" died", src_path)
					return
				}

				log.Printf("Watcher error for \"%s\": %s", src_path, err)

			case conn := <-connCh:
				// New websocket connection.
				conns = append(conns, conn)
			}
		}
	}()

	http.HandleFunc(api_path + "/script.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeContent(w, r, "script.js", server_start_time, strings.NewReader(update_script))
	})

	http.HandleFunc(api_path + "/socket", func(w http.ResponseWriter, r *http.Request) {
		conn, err := websocket.Accept(w, r, nil)
		if err != nil {
			log.Printf("Failed to upgrade to websocket: %s", err)
			return
		}
		conn.CloseRead(context.Background())
		connCh <- conn
	})

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("File requested: %s", r.URL.Path)

		// Try to open the file at the given path and then stat it.
		file, err := http.Dir(root).Open(r.URL.Path)
		if err != nil {
			log.Printf("Failed to open file: %s", err)

			if errors.Is(err, fs.ErrNotExist) {
				http.NotFound(w, r)
			} else {
				http.Error(w, "500 internal server error", http.StatusInternalServerError)
			}
			return
		}
		stat, err := file.Stat()
		if err != nil {
			log.Printf("Failed to stat file: %s", err)
			http.Error(w, "500 internal server error", http.StatusInternalServerError)
			return
		}

		// If the file is a directory, then open "index.html" inside of it instead.
		if stat.IsDir() {
			file.Close()
			file, err = http.Dir(root).Open(filepath.Join(r.URL.Path, "index.html"))
			if err != nil {
				log.Printf("Failed to stat file: %s", err)
				if errors.Is(err, fs.ErrNotExist) {
					http.NotFound(w, r)
				} else {
					http.Error(w, "500 internal server error", http.StatusInternalServerError)
				}
				return
			}
			stat, err = file.Stat()
			if err != nil { http.NotFound(w, r); return }
		}
		defer file.Close()

		if(isHtml(stat.Name(), stat.IsDir(), file)) {
			w.Header().Set("Content-Type", "text/html")
			w.Header().Set("Last-Modified", stat.ModTime().UTC().Format(http.TimeFormat))

			z := html.NewTokenizer(file)
			for {
				// Get another token and handle errors.
				tt := z.Next()
				if tt == html.ErrorToken {
					if z.Err() != io.EOF {
						log.Printf("Failed to tokenize html for \"%s\": %s", r.URL.Path, err)
						http.Error(w, "500 internal server error", http.StatusInternalServerError)
						return
					}
					break
				}

				// Write the current raw HTML token.
				if _, err := w.Write(z.Raw()); err != nil {
					log.Printf("Failed to write html for \"%s\": %s", r.URL.Path, err)
					return
				}

				// Inject the script if at the end of the body element.
				tagname, _ := z.TagName()
				if tt == html.StartTagToken && string(tagname) == "body" {
					if _, err := io.WriteString(w, injected_html); err != nil {
						log.Printf("Failed to write html for \"%s\": %s", r.URL.Path, err)
						return
					}
				}
			}
		} else {
			// It's not an html file.
			http.ServeContent(w, r, stat.Name(), stat.ModTime(), file)
		}
	})

	log.Println("Failed to start http server: %s", http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
