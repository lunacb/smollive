module codeberg.org/lunacb/smollive

go 1.19

require (
	github.com/fsnotify/fsnotify v1.6.0
	golang.org/x/net v0.8.0
	nhooyr.io/websocket v1.8.7
)

require (
	github.com/klauspost/compress v1.10.3 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
