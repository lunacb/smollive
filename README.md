# smollive

A local HTTP server that watches for changes to the filesystem and makes your
web-browser update alongside those changes through a script injected into
requested HTML pages.

Cozy for html static site writing.

Don't use for more than ten hours at a time. Hydrate during breaks.

# Running Locally

```sh
go run . [options]
```

# Installing

```sh
GOBIN=/usr/local/bin go install .
```

# Command-line Options

* -f, --force-reload - Forces the page to reload itself after every file update.
  If this option isn't set, then some changes like stylesheets and images can
  be dynamically updated on the page without it being fully reloaded.

* -r, --root PATH - Set the directory to be served by the filesystem. Defaults
  to the current directory.

* -p, --port PORT - Set the port for the http server. Defaults to 8888.

* -s, --sleep-time DURATION - Specifies how long the server should sleep for
  before propagating file changes. Making this longer means giving more time
  for any file changes to accumulate. Defaults to 100ms.

* --api-path PATH - Set the base path used by the server to provide the
  esources needed for client-side updating. You can change this if you want to
  use that path for the actual site. Defaults to "_smollive".

* --cache-param-name NAME - Name of a URL query parameter used to prevent
  caching. Defaults to "_smollive_anticache".

* --no-checksums - If set, the server will not use file checksums to decide
  what's been changed, and will interpret any filesystem event as a flie-change
  instead.

## Source Update Command

A command can be configured to run when any file in a specified source
directory changes.

* -C, --src-cmd COMMAND - An update command that will be run when the source
  directory changes.

* -D, --src-cwd PATH - The working directory of the update command. Defaults to
  the source directory.

* -P, --src-path PATH - The source path to be montored.
